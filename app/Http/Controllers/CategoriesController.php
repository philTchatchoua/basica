<?php
/*
    Chemin : app/Http/Controllers/CategoriesController.php
    Description: Controller des caztegories
    Données disponible: -- // --
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page as PageMdl;
use Illuminate\Support\Facades\DB;
use App\Models\Categorie as CategorieMdl;


class CategoriesController extends Controller
{
    /**
    * type: function
    * nom: show
    * Desc: details de la categorie
    */
    public function show(int $id)
    {   
        $categorie = CategorieMdl::find($id);
        return view('categories.show', compact('categorie','page','posts'));
    }
}
