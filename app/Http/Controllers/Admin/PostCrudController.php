<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/post');
        $this->crud->setEntityNameStrings('post', 'posts');


        /* Ajout du champs image */
        $this->crud->addField(
            [
                'label' => "Image du Projet",
                'name' => "image",
                'type' => 'image',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 1,
                'disk' => 'uploads',
                'prefix' => 'uploads/images'
            ]

        );

        /*Champ de la liste des categories */
        $this->crud->addField(
            [ // Select2
                'label' => "Liste des Categories",
                'type' => 'select2',
                'name' => 'categories_id',
                'entity' => 'categorie', 
                'attribute' => 'nom', 
                // optional
                'model' => "App\Models\Categorie", // foreign key model
                'default' => 2,
                'options'   => (function ($query) {
                    return $query->orderBy('nom', 'ASC')->get();
                }), 

            ]
        );


        /*Champ de la liste des clients */
        $this->crud->addField(
            [ // Select2
                'label' => "Liste des Clients",
                'type' => 'select2',
                'name' => 'clients_id', 
                'entity' => 'client', 
                'attribute' => 'nom',

                // optional
                'model' => "App\Models\Client", // foreign key model
                'default' => 2, 
                'options'   => (function ($query) {
                    return $query->orderBy('nom', 'ASC')->get();
                }), 

            ]
        );
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PostRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
