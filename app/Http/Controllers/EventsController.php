<?php
/*
    Chemin : app/Http/Conrollers/EventsController.php
    Description: Controller des evenements(flux rss)
    Données disponible: -- // --
*/

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventsController extends Controller
{   
    /**
    * type: function
    * nom: show
    * Desc: permet d avoir les elelments du flux rss
    */
    public function show(Request $request, Event $event)
    {
        return $event;
    }
}
