<?php
/*
    Chemin :app/Http/Controllers/PostsController.php
    Description: Controller des poss
    Données disponible: -- // --
*/

namespace App\Http\Controllers;

use App\Models\Categorie as CategorieMdl;
use App\Models\Post as PostMdl;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * type: function
     * nom: show
     * Desc: Details d'un posts
     * in  
     */
     public function show(int $id)
     {
        $post = PostMdl::find($id);
        $categories = CategorieMdl::all();
        $posts = PostMdl::orderBy('id','desc')->take(3)->get();
        return view('posts.show', compact('post','categories','posts'));
     }

    /**
     * type: function
     * nom: show
     * Desc:liste des posts (elle envoie la liste des post recents afficher coe widget sur le details d'unpost)
     */
    public function index()
    {
        $posts = PostMdl::all();
        return view('posts.show', compact('posts'));
    }
}
