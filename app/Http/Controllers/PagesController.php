<?php
/*
    Chemin :./app/Http/Controllers/PagesController.php
    Description: Controller des pages
    Données disponible: $id, $slug

*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page as PageMdl;
use App\Models\Post as PostMdl;
use App\Models\Slide as SlideMdl;
use App\Models\Client as ClientMdl;
use App\Models\Projet as ProjetMdl;
use Thujohn\Twitter\Facades\Twitter;

class PagesController extends Controller
{
   /**
    * function : show
    * Description: return le details d'une page 
    */

    public function show($id = 1)
    {
        $page = PageMdl::find($id);
        $clients = ClientMdl::All(); 

        if($page->id == 1):
            $projets = ProjetMdl::orderBy('created_at','desc')->take(6)->get();
            $posts = PostMdl::orderBy('id')->take(3)->get();
            $tweets = Twitter::getUserTimeline(['screen_name' => 'PhilTchatchoua', 'count' => 4, 'format' => 'object']);
            return view('pages.show',compact('page','projets','posts', 'clients','tweets'));
            
        elseif($page->id == 2):

            $projets = ProjetMdl::orderBy('created_at','desc')->take(3)->get();
            return view('pages.show', compact('page', 'projets'));

        elseif ($page->id == 3) :

            $posts = PostMdl::orderBy('id')->take(4)->paginate(4);
            return view('pages.show', compact('page', 'posts'));

        else:
            return view('pages.show',compact('page'));
        endif;

        //return view('pages.show', compact('page'));
    }
}
