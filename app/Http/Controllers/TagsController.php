<?php
/*
    Chemin : app/Http/Controllers/TagController.php
    Description: Controller des tags
    Données disponible: -- // --
*/

namespace App\Http\Controllers;

use App\Models\Tags as TagsMdl;
use Illuminate\Http\Request;

class TagsController extends Controller
{
        /**
         * type : function
         * Nom  : index
         * Desc : listes des tags
         */
        public function index()
        {
            $tags = TagsMdl::all();
            return view('projets.show', compact('tags'));
        }

}
