<?php
/*
    Chemin :./app/Http/Controllers/ProjetsController.php
    Description: Controlleur des projets
    Données disponible: -- // --
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tags as TagsMdl;
use Illuminate\Support\Facades\DB;
use App\Models\Projet as ProjetMdl;

class ProjetsController extends Controller
{
    /**
     * function public 
     * nom : show
     * Desc: Details d'un projet
     */

    public function show(int $id, string $slug)
    {
        $projet = ProjetMdl::find($id);

        //Ici je recupère les ou le tag(s) du projet
        $nameTags = DB::table('tags')
                ->join('projets_has_tags','tags.id','=','tags_id')
                ->join('projets','projets_id','=','projets.id')
                ->select('tags.nom')
                ->where('projets.id','=',$id)
                ->get();
        
 

        //je recupère les projets qui ont pour tags les se rouvant dans la variable => $nameTags 
         foreach($nameTags as $key =>$value) {
           $nameProjets = DB::table('clients')
                ->join('projets','clients.id','=','projets.id')
                ->join('projets_has_tags', 'projets.id', '=', 'projets_id')
                ->join('tags', 'tags_id', '=', 'tags.id')
                ->select('projets.image', 'projets.id','projets.titre','clients.nom')
                ->where('tags.nom', 'like', '%'.$nameTags[$key]->nom.'%')
                ->where('projets.id', '<>', $id)
                ->get();
        
         }

             return view('projets.show',compact('projet', 'nameProjets'));
        }

        /**
         * type: function public
         * nom: ajaxMorePortofolio
         * Desc: liste des projets
         */
        public function ajaxMorePortofolio(Request $request)
        {   
            $offset = $request->get('offset');
            $projets = ProjetMdl::orderBy('created_at', 'desc')->take(3)->offset($offset)->get();
            return view('projets.listeProjets', compact('projets'));

        }
   
}
