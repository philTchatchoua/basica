<?php
/*
    Chemin :app/Models/Event.php
    Description: Models des events
    Données disponible: -- // --
*/

namespace App\Models;

use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;
use Illuminate\Database\Eloquent\Model;

class Event extends Model implements Feedable
{
    protected $table = 'posts';
    /**
     * type: function public
     * nom:  toFeedItem
     * Desc: definit les elements qui seront definis dans le flux Rss
     */
    public function toFeedItem()
    {
        return FeedItem::create()
            ->id($this->id)
            ->title($this->titre)
            ->summary($this->texteLead)
            ->updated($this->updated_at)
            ->link($this->link)
            ->author($this->clients_id);
    }

    /**
    * type: function public static
    * nom: getFeedItems
    * Desc:Permet de selection des last feed et fixer uen limite (flux rss)
    */
    public static function getFeedItems()
    {
        return static::orderBy('created_at','desc')->limit(7)->get();
    }

    /**
    * type: function public
    * nom: getLinlAttributes
    * Desc:Dtermine la route des feed
    */
    public function getLinkAttribute()
    {
        return route('feed.show', $this);
    }
}
