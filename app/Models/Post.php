<?php
/*
    Chemin : app/Models/Post.php
    Description: Model des posts
    Données disponible: -- // --
*/
namespace App\Models;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Post extends Model
{
    use CrudTrait;
    protected $table ='posts';
    protected $guarded=[];
    

    /**
     * type : function
     * name : categories
     * Desc: liaison 1-N (categorie - posts)
     */
     public function categorie()
     {
         return $this->belongsTo('App\Models\Categorie','categories_id');
         
     }

    /**
     * type : function
     * name : categories
     * Desc: liaison 1-N (categorie - posts)
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'clients_id');
    }



    /**
     * type: function
     * nom: setImageAttribute
     * Desc: mise en place du système  de telechargement
     */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = 'uploads';
        $destination_path = "uploads/images";


        if ($value == null) {

            \Storage::disk($disk)->delete($this->{$attribute_name});

            $this->attributes[$attribute_name] = null;
        }

        if (starts_with($value, 'data:image')) {

            $image = \Image::make($value)->encode('jpg', 90);

            $filename = md5($value . time()) . '.jpg';

            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());

            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = $public_destination_path . '/' . $filename;
        }
    }


    /**
     * type: static function
     * nom: boot
     * Desc:Permet de supprimer les images dans le backoffice et dans le disk de l'appli (emplacemnt ou sont stocker les images)
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            \Storage::disk('uploads')->delete($obj->image);
        });
    }

}
