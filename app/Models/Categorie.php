<?php
/*
   Chemin :app/Models/Categorie.php
   Description: Models des categories
   Données disponible: -- // --
*/

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use CrudTrait;
    protected $table ='categories';
    protected $guarded=[];


    /**
     * type: function
     * nom: posts
     * Desc: liaison 1-N (catagorie - posts)
     */
     public function posts()
     {
        return $this->hasMany('App\Models\Post','categories_id');
     }
    
}
