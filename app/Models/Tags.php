<?php
/*
    Chemin : app/Models/Tags.php
    Description: Model des Tags
    Données disponible: -- // --
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Tags extends Model
{
    use CrudTrait;
    protected $table ='tags';
    protected $guarded =[];

    /**
     * type : function
     * nom  : projets
     * desc : Permet la mise liaison entre la table Tags et projet N-M
     */
    public function projets()
    {
        return $this->belongsToMany('App\Models\Projet','projets_has_tags', 'projets_id', 'tags_id');
    }
}
