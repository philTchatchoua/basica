<?php
/*
    Chemin :./app/Models/Client.php
    Description: Model des clients
    Données disponible: -- // --
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Client extends Model
{
    use CrudTrait;
    protected $table ='clients';
    protected $guarded=[];
    
    /**
     * function : projet
     * desc : Permet d'associer le client au Projet
     */
    public function projet()
    {
        return $this->hasMany('App\Models\Projet');
    }
    
    /**
     * type: function public 
     * nom: posts
     * Desc: liaison 1-N (catagorie - posts)
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'clients_id');
    }
}
