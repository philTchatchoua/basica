<?php
/*
    Chemin : app/Models/Page.php
    Description: Model des pages
    Données disponible: -- // --
*/

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use CrudTrait;
    protected $table='pages';
    protected $guarded=[];
    
}
