/**
 * Chemin : js/portofolio/olderPortofolio.js
 * description : scripts dynamique
 * Variable disponible : -- // -- 
 */

 $(function(){

     var baseUrl = $('body').attr('data-baseUrl');
     var offsetCalcule = 3;

     $('#More-works').click(function(e){
         e.preventDefault();
         
         $.ajax({
             url: baseUrl + '/ajax/More-Portofolio',
             data :{
                 offset:offsetCalcule
             },
             method:'get',
             success : function(reponsePHP){
                 $('#listeProjets').append(reponsePHP)
                                   .find('.projet-preview:nth-last-of-type(-n+3)')
                                   .hide().fadeIn();

                 offsetCalcule +=3;                 
             },
             error : function(){
                 alert('Problèmre durant la transaction !!!');
             }
         });
     });
     
 });