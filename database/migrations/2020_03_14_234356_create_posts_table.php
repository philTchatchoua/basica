<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre');
            $table->longText('texteLead');
            $table->longText('texteSuite');
            $table->integer('tri')->unique()->nullable();
            $table->string('image');
            $table->timestamps();
            $table->unsignedBigInteger('clients_id');
            $table->unsignedBigInteger('categories_id');
            $table->foreign('clients_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('categories_id')->references('id')->on('categories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
