<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre')->nullable();
            $table->longText('texte');
            $table->integer('tri')->unique()->nullable();
            $table->string('image');
            $table->integer('status_image')->nullable();
            $table->unsignedBigInteger('clients_id');
            $table->timestamps();
            $table->foreign('clients_id')->references('id')->on('clients')->onDelete('cascade');  //Foreign key(clients_id)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets');
    }
}
