<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetsHasTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projets_has_tags', function (Blueprint $table) {
            $table->unsignedBigInteger('projets_id');
            $table->unsignedBigInteger('tags_id');
            $table->timestamps();
            $table->primary(['projets_id','tags_id']);
            $table->foreign('projets_id')->references('id')->on('projets')->onDelete('cascade');
            $table->foreign('tags_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets_has_tags');
    }
}
