<?php

use App\Http\Controllers\PostsController;
use App\Http\Controllers\ProjetsController;

/*-----------------------------------------------------------------------------
                            View Composer
-------------------------------------------------------------------------------*/

//View dynammique Nav

View::composer('pages.index',function($view){
    $view->with('pages',App\Models\Page::all());
});

/*-----------------------------------------------------------------------------
                            Web Routes
-------------------------------------------------------------------------------*/

//Route des pages
Route::get('/','PagesController@show')->name('pages.accueil');

Route::get('pages/{id}/{slug}.html','PagesController@show')->name('pages.show');

//Routes Projet
Route::get('projets/{id}/{slug}.html','ProjetsController@show')->name('projets.show');

//Routes Posts
Route::get('posts/{id}/{slug}.html','PostsController@show')->name('posts.show');

//Routes categories
Route::get('categories/{id}/{slug}.html','CategoriesController@show')->name('categories.show');


/*-----------------------------------------------------------------------------
                            Ajax Routes
-------------------------------------------------------------------------------*/

Route::get('ajax/More-Portofolio','ProjetsController@ajaxMorePortofolio')->name('ajax.More.Portofolio');

/*-------------------------------------------------------------------------------
                    Routes flux 
--------------------------------------------------------------------------------*/
Route::get('/feed/{event}', 'EventsController@show')
    ->name('feed.show');
Route::feeds();


