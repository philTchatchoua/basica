{{--
    Chemin :/resources/views/templates/partials/_head.blade.php
    Description: Affiche la liste des pages donc (Navigateur)
    Données disponible: 
                    -- Array(Array(obj[id,titre, texte, tri, created_at, updated_at]))
--}}
<ul class="nav navbar-nav navbar-right">
    @foreach($pages as $page)
        @if ($page->id === 1)
            <li class="active">
                <a href="{{ URL::route('pages.show',['id'=>$page->id, 'slug'=>Str::slug($page->titre, '-')]) }}">
                    {{$page->titre}}
                </a>
            </li>
        @else
            <li class="">
                <a href="{{ URL::route('pages.show',['id'=>$page->id, 'slug'=>Str::slug($page->titre, '-')]) }}">
                    {{$page->titre}}
                </a>
            </li>
        @endif
       
    @endforeach          
</ul>
