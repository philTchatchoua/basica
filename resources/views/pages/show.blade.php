{{--
    Chemin :/resources/views/pages/show.blade.php
    Description: template du details d'une page
    Données disponible:
                    -- $pages Array(obj[id, titre, texte, tri, created_at, updated_at])
                    -- $projets Array(obj[id, titre, texte, tri, image, created_at, updated_at, clients_id])
                    -- $posts Array(obj[id, titre, texteLead,texteSuite, tri, image,created_at, updated_at, clients_id,categories_id])

--}}

@extends('templates.default')
@section('titre')
    {{$page->titre}}
@stop
@section('content')
 @if ($page->id === 1)

     <section id="main-slider" class="no-margin">
    
    <div class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1"></li>
            <li data-target="#main-slider" data-slide-to="2"></li>
        </ol>
            <div class="carousel-inner" id="test">
                 @foreach($projets as $projet)
                    <div class="item @if($loop->first) active @endif" style="background-image: url({{ asset($projet->image)}}) " >
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                <h2 class="animation animated-item-1">{{$projet->titre}}</h2>
                                <p class="animation animated-item-2">{{ Str::words($projet->texte,50, ' ...')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                 @endforeach
           </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="icon-angle-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="icon-angle-right"></i>
        </a>      
    </section>    
        <!--/#main-slider-->
        <!-- Our Portfolio -->
        <div class="section section-white">
            <div class="container">
                <div class="row">
                <div class="section-title">
                    <h1>Our Recent Works</h1>
                </div>
                    <ul class="grid cs-style-3">
                        {{-- BOUCLE POUR LISTER LES PROJETS --}}
                        @foreach ($projets as $projet)
                            <div class="col-md-4 col-sm-6">
                                <figure>
                                    <img src="{{ asset($projet->image) }}" alt="img04">
                                    <figcaption>
                                    <h3>{{$projet->titre}}</h3>
                                    <span>{{$projet->client->nom}}</span>
                                        <a href="{{ URL::route('projets.show',['id'=>$projet->id,'slug'=>Str::slug($projet->titre, '-')]) }}">Take a look</a>
                                </figcaption>
                                </figure>
                            </div>
                        @endforeach  
                    </ul>
                 </div>
            </div>
        </div>
        <!-- Our Portfolio -->
    <hr>
    <!-- Our Articles -->
    <div class="section">
      <div class="container">
        <div class="row">
          <!-- Featured News -->
          <div class="col-sm-6 featured-news">
            <h2>Latest Blog Posts</h2>
            {{-- Boucle foreach --}}
            @foreach ($posts as $post )
              <div class="row">
                <div class="col-xs-4"><a href="{{ URL::route('posts.show',['id'=>$post->id,'slug'=>Str::slug($post->titre, '-')]) }}"><img src="{{ asset($post->image) }}" alt="Post Title"></a></div>
                  <div class="col-xs-8">
                  <div class="caption"><a href="{{ URL::route('posts.show',['id'=>$post->id,'slug'=>Str::slug($post->titre, '-')]) }}">{{$post->titre}}</a></div>
                    <div class="date">  {{\Carbon\Carbon::parse($post->created_at)->format('d-M-Y') }}</div>
                    <div class="intro"> {{ Str::words($post->texteLead,20, ' ...')}}. <a href="{{ URL::route('posts.show',['id'=>$post->id,'slug'=>Str::slug($post->titre, '-')]) }}">Read more...</a></div>
                </div>
              </div> 
            @endforeach
          </div>
          <!-- End Featured News -->
          <!-- Latest News FB -->
          <div class="col-sm-6 latest-news">
            <h2>Lastest FaceBook/Twitter News</h2>
            @foreach($tweets as $tweet)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="caption">
                            <a href="{{'http://twitter.com/'. $tweet->user->screen_name }}">{{$tweet->user->name}}</a>
                        </div>
                        <div class="date">{{\Carbon\Carbon::parse($tweet->created_at)->format('d-M-Y') }}</div>
                        <div class="intro">{{$tweet->text}}</div>
                        <a href="{{'http://twitter.com/'. $tweet->user->screen_name }}">Read more...</a>
                    </div>
                </div>
            @endforeach  
          </div>
          <!-- End Featured News -->
        </div>
      </div>
    </div>
   @elseif($page->id === 2)
    
      @include('projets.index')
      @section('scripts')
        <script src="{{ asset('js/portofolio/olderPortofolio.js') }}"></script>
      @stop

   @elseif($page->id === 3)

     @include('posts.index')
   
  @else
      @include('contacts.index')
  @endif
@stop 
