{{--
    Chemin :/resources/views/templates/partials/_script.blade.php
    Description: partie javascript du template par default
    Données disponible: -- // --
--}}

<!-- javascript -->
<script src="{{ asset('http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Scrolling Nav JavaScript -->
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/scrolling-nav.js') }}"></script>

<!-- scritps dynamique -->
@yield('scripts')
