{{--
    Chemin :/resources/views/templates/partials/_header.blade.php
    Description: header du templates par default
    Données disponible: -- // --
--}}

<header class="navbar navbar-inverse navbar-fixed-top" role="banner" style="padding-bottom: 40px;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::route('pages.accueil')}}"><img src="{{ asset('img/logo.png') }}" alt="Basica"></a>
        </div>
        <div class="collapse navbar-collapse">
            @include('pages.index')
        </div>
    </div>
</header>
