{{--
    Chemin :./resources/views/templates/defaults.blade.php
    Description: Templates par default
    Données disponible: -- // --
--}}

<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="fr">

  <!-- <head> -->
  <head>
      @include('templates.partials._head')
  </head>
  <!-- <head> -->
    
  <body data-baseUrl ="{{url('/')}}">

    <!-- <header> -->
        @include('templates.partials._hearder')
    <!-- </header> -->

    <!-- <body> -->
        @include('templates.partials._body')
    <!-- </body> -->

    <!-- <footer> -->
        @include('templates.partials._footer')
    <!-- </footer> -->

    <!-- <Javascript>-->
        @include('templates.partials._script')
        
  </body>
  <script>
      // je gere ici le temps de  rotation des sliders
      $('#test').carousel({
             interval:3050
    })
  </script>

</html>
