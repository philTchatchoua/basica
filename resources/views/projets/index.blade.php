{{--
    Chemin :/resources/views/projets/index.blade.php
    Description: listes des projets
    Données disponible:
                -- $projets Array[Array(obj[ is, titre, textes, tri, image, created_at, update_at])]
--}}

<div class="section section-breadcrumbs" style="margin-top:90px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h1>Our {{$page->titre}}</h1>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
        <div class="col-sm-12">
                <h2>We are leading company</h2>
                <h3>Specializing in Wordpress Theme Development</h3>
                <p>
                   {{ $page->texte }}
                </p>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <ul class="grid cs-style-3" id="listeProjets">
                @include('projets.listeProjets')
            </ul>
        </div>
        <ul class="pager">
            <li><a href="#" id="More-works">More works</a></li>
        </ul>

    </div>
</div>

