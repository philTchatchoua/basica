{{--
    Chemin :/resources/views/projets/show.blade.php
    Description: Details des projets 
    Données disponible:
                    -- $projet Array(Obj[id, titre , texte, tri, image,created_at, update_at, clients_id])
--}}


@extends('templates.default')

@section('titre')
{{ $projet->titre }}
@stop

@section('content')

<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Product Details</h1>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <!-- Product Image & Available Colors -->
            <div class="col-sm-6">
                <div class="product-image-large">
                    <img src="{{ asset($projet->image) }}" alt="Item Name">
                </div>
                <div class="colors">
                    <span class="color-white"></span>
                    <span class="color-black"></span>
                    <span class="color-blue"></span>
                    <span class="color-orange"></span>
                    <span class="color-green"></span>
                </div>
            </div>
            <!-- End Product Image & Available Colors -->
            <!-- Product Summary & Options -->
            <div class="col-sm-6 product-details">
                <h2>{{$projet->titre}}</h2>
                <h3>Quick Overview</h3>
                <p>
                    {{ $projet->texte }}
                </p>
                <h3>Project Details</h3>
                <p><strong>Client: </strong>{{ $projet->client->nom }}</p>
                <p><strong>Date: </strong> {{\Carbon\Carbon::parse($projet->created_at)->format('d-M-Y') }}</p>
                <p><strong>Tags:</strong>
                    <?php  foreach ($projet->tags as $tag):
                        $nberDuTag = $tag->pivot->tags_id;  //ici je recupère l'id du tag dans la intermediaire
                        $eletTags = App\Models\Tags::find($nberDuTag);  //je selectionne l'élément correspodant dans les tags
                    ?>      
                        <a href="">{{ $eletTags->nom}}</a>
                    <?php endforeach ?>    
                </p>
            </div>
           <!-- End Product Summary & Options -->        
        </div>
    </div>
</div>
<hr>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                 <h1>Similar Works</h1>
            </div>
            <ul class="grid cs-style-3">
                <?php  foreach ($nameProjets as $key => $value):?>
                    <div class="col-md-3 col-sm-6">
                        <figure>
                        <img src="<?php  echo asset($nameProjets[$key]->image)?>" alt="img04">
                            <figcaption>
                                <h3><?php echo $nameProjets[$key]->titre?></h3>
                                <span><?php echo $nameProjets[$key]->nom?></span>
                                <a href="<?php echo URL::route('projets.show',['id'=> $nameProjets[$key]->id,'slug'=>Str::slug($nameProjets[$key]->titre, '-')]) ?>">Take a look</a>
                            </figcaption> 
                        </figure>
                    </div> 
                <?php  endforeach; ?>
            </ul>
        </div>
    </div>
</div>
@stop

