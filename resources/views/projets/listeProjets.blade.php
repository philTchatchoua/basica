{{--
    Chemin :/resources/views/projets/listeprojets.blade.php
    Description: liste des projets
    Données disponible:
                    -- $projet Array(Array(obj[id, titre, sous_titre, texte, image, created_at, updated_at]))
--}}

    @foreach($projets as $projet)
        <div class="col-md-4 col-sm-6 projet-preview">
            <figure>
                <img src="{{ asset($projet->image) }}" alt="img04">
                <figcaption style="height:95px">
                    <h3>{{$projet->titre}}</h3>
                    <span>{{$projet->client->nom}}</span>    
                    <a href="{{ URL::route('projets.show',['id'=>$projet->id, 'slug'=>Str::slug($projet->titre, '-')]) }}">Take a look</a>
                </figcaption>
            </figure>
        </div>
    @endforeach 

