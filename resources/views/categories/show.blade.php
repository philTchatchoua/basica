{{--
    Chemin :/resources/views/categories/show.blade.php
    Description: details de la categorie
    Données disponible:
                    -- $categorie Array(obj[id, nom, tri, created_at, updated_at])
--}}
@extends('templates.default')
@section('titre')
    {{ $categorie->nom }}
@stop
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h1>Our blog</h1>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            @foreach ($categorie->posts as $post)
                 <!-- Blog Post Excerpt -->
                <div class="col-sm-6">
                    <div class="blog-post blog-single-post">
                        <div class="single-post-title">
                            <h2>{{$post->titre}}</h2>
                        </div>

                        <div class="single-post-image">
                            <img src="{{ asset($post->image) }}" alt="Post Title">
                        </div>

                        <div class="single-post-info">
                            <i class="glyphicon glyphicon-time"></i>{{\Carbon\Carbon::parse($post->created_at)->format('d-M-Y') }}<a href="#" title="Show Comments"> <i class="glyphicon glyphicon-comment"></i>11</a>
                        </div>

                        <div class="single-post-content">
                            <p>
                              {{ $post->texteLead }}
                            </p>
                        <a href="{{ URL::route('posts.show',['id'=> $post->id,'slug'=>Str::slug($post->titre, '-')]) }}" class="btn">Read more</a>
                        
                        </div>
                    </div>
                </div>
                 <!-- End Blog Post Excerpt -->   
            @endforeach
<!-- End Blog Post Excerpt -->
            <!-- Pagination -->
            <div class="pagination-wrapper ">
               
            </div>
        </div>
    </div>
</div>
