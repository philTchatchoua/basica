{{--
    Chemin :/resources/views/posts/show.blade.php
    Description: details d'un posts
    Données disponible: 
                    -- $post Array(obj[id,titre, texteLead, texteSUite,tri, image, cretated_at, updated_at, clients_id, categories_id])
                    -- $categories Array(Array(obj[id, nom, tri, created_at, updated_at]))
--}}

@extends('templates.default')
@section('titre')
   {{ $post->titre }}
@stop
@section('content')
 <!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Blog Post</h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <!-- Blog Post -->
            <div class="col-sm-8">
                <div class="blog-post blog-single-post">
                    <div class="single-post-title">
                    <h2>{{$post->titre}}</h2>
                    </div>

                    <div class="single-post-image">
                        <img src="{{ asset($post->image) }}" alt="Post Title">
                    </div>
                    <div class="single-post-info">
                        <i class="glyphicon glyphicon-time"></i>{{\Carbon\Carbon::parse($post->created_at)->format('d-M-Y') }}<a href="#" title="Show Comments"> <i class="glyphicon glyphicon-comment"></i>11</a>
                    </div>
                    <div class="single-post-content">
                        <h3>Lorem ipsum dolor sit amet</h3>
                        <p>
                            {{ $post->texteLead }}
                        </p>
                        
                        <h3>Sed sit amet metus sit</h3>
                        <p>
                            {{$post->texteSuite }}
                        </p>
                    </div>
                </div>
            </div>
            <!-- End Blog Post -->
            <!-- Sidebar -->
            <div class="col-sm-4 blog-sidebar">
                <h4>Recent Posts</h4>
                <ul class="recent-posts">
                    @foreach($posts as $post)
                         <li>
                            <a href="{{ URL::route('posts.show',['id'=> $post->id,'slug'=>Str::slug($post->titre, '-')]) }}">{{$post->titre}}</a>    
                        </li>
                    @endforeach  
                </ul>
                <h4>Categories</h4>
                <ul class="blog-categories">
                        @foreach($categories as $categorie)
                             <li>
                                 <a href="{{ URL::route('categories.show',['id'=>$categorie->id,'slug'=>Str::slug($categorie->nom,'-')]) }}">{{ $categorie->nom }}</a> 
                            </li>
                        @endforeach
                </ul>
                
            </div>
            <!-- End Sidebar -->
        </div>
    </div>
</div>
@stop