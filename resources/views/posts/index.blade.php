{{--
    Chemin :/resources/views/posts/index.blade.php
    Description: listes des posts
    Données disponible:
                    -- $posts Array(Array(obj[id, titre textelead, textesuite,tri, image, created_at, updated_at,clients_id, categories-id]))
--}}


<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h1>Our {{$page->titre}}</h1>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            @foreach ($posts as $post) 
                 <!-- Blog Post Excerpt -->
                <div class="col-sm-6">
                    <div class="blog-post blog-single-post">
                        <div class="single-post-title">
                            <h2>{{$post->titre}}</h2>
                        </div>

                        <div class="single-post-image">
                            <img src="{{ asset($post->image) }}" alt="Post Title">
                        </div>

                        <div class="single-post-info">
                            <i class="glyphicon glyphicon-time"></i>{{\Carbon\Carbon::parse($post->created_at)->format('d-M-Y') }}<a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
                        </div>

                        <div class="single-post-content" >
                            <p style="height:150px">
                              {{ $post->texteLead }}
                            </p>
                        <a href="{{ URL::route('posts.show',['id'=> $post->id,'slug'=>Str::slug($post->titre, '-')]) }}" class="btn">Read more</a>
                        
                        </div>
                    </div>
                </div>
                 <!-- End Blog Post Excerpt -->   
            @endforeach      
<!-- End Blog Post Excerpt -->
            <!-- Pagination -->
            <div class="pagination-wrapper ">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>
